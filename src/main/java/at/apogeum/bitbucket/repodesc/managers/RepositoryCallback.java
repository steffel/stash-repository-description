package at.apogeum.bitbucket.repodesc.managers;

import com.atlassian.bitbucket.repository.Repository;

/**
 * @author Stephan Bechter <stephan@apogeum.at>
 */
public interface RepositoryCallback {

    void handle(Repository repository);
}
