# Repodesc changelog

## Version 2.5.0
* 

## Version 2.4.0
* work again with bitbucket server versions 5.5.x and newer

## Version 2.3.1
* -

## Version 2.3.0
* Added additional data from each repo's latest commit (e.g., commit message, time, author) (Thanks to G. Sylvie Davies [bit-booster.com])
* Made columns sortable.
* Works on IE10+ now!

## Version 2.2.1
* Same as 2.2.0 but used now correct version id number

## Version 2.2.0
* Fixed compatibility issues with newer BB versions

## Version 2.1.0
* Finished migration to Bitbucket Server

## Version 2.0.0
* Migration for Bitbucket Server 4.0.0 - NOT PUBLISHED

## Version 1.2.1
* Bugfix: installation failed on stash running with JAVA < 1.8

## Version 1.2.0
* Compatible with Atlassian DataCenter
* Use JS navbuilder to get base rest URL

## Version 1.1.0
* Use text area for the repository description
* Add description to private repositories

## Version 1.0.0
* Initial version

